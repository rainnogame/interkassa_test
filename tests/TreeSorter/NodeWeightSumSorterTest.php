<?php


namespace Interkassa_test\Test\TreeSorter;

use Interkassa_test\TreeSorter\NodeWeightSumSorter;
use Interkassa_test\WeightNode\WeightNode;
use PHPUnit\Exception;
use PHPUnit\Framework\TestCase;

final class NodeWeightSumSorterTest extends TestCase
{
    public function wrongSumConstProvider()
    {
        return [
            [1.1],
            [null],
        ];
    }

    public function nodeArraysChunkTestProvider()
    {
        return [
            [
                [
                    new WeightNode(1, null),
                    new WeightNode(4, null),
                    new WeightNode(3, null),
                ],
                5,
                [
                    [
                        new WeightNode(1, null),
                        new WeightNode(4, null),
                    ],
                    [
                        new WeightNode(3, null),
                    ],
                ],
            ],
            [
                [
                    new WeightNode(6, null),
                    new WeightNode(7, null),
                ],
                5,
                [
                    [
                    ],
                    [
                        new WeightNode(6, null),
                        new WeightNode(7, null),
                    ],
                ],
            ],
            [
                [
                    new WeightNode(1, null),
                    new WeightNode(2, null),
                ],
                5,
                [
                    [
                        new WeightNode(1, null),
                        new WeightNode(2, null),
                    ],
                    [

                    ],
                ],
            ],
        ];
    }

    /**
     * @param $array
     * @param $sumCont
     * @param $expect
     * @dataProvider nodeArraysChunkTestProvider
     */
    public function testChunkBySum($array, $sumCont, $expect)
    {
        $result = NodeWeightSumSorter::chunkBySum($array, $sumCont);

        self::assertEquals($expect, $result);
    }

    public function testChunkBySumOnEmptyData()
    {
        $result = NodeWeightSumSorter::chunkBySum([], 1);
        self::assertEquals($result, [[], []]);
    }

    /**
     * @param $wrongSumConst
     * @dataProvider wrongSumConstProvider
     * @expectedException Exception
     */
    public function testChunkBySumOnWrongSumCont($wrongSumConst)
    {
        NodeWeightSumSorter::chunkBySum([], $wrongSumConst);
    }

    public function nodeArraysProvider()
    {
        return [
            [
                [
                    new WeightNode(1, null),
                    new WeightNode(4, null),
                    new WeightNode(3, null),
                ],
                [
                    new WeightNode(1, null),
                    new WeightNode(3, null),
                    new WeightNode(4, null),
                ],
            ], [
                [

                ],
                [

                ],
            ],
        ];
    }

    /**
     * @param $array
     * @param $expect
     * @dataProvider nodeArraysProvider
     */
    public function testWeightModesInsertSort($array, $expect)
    {
        NodeWeightSumSorter::weightModesInsertSort($array);
        $this->assertEquals($array, $expect);
    }

}