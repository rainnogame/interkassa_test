<?php


namespace Interkassa_test\Test\TreeSorter;


use Interkassa_test\LinkList\LinkList;
use Interkassa_test\TreeSorter\NodeWeightSumSorter;
use Interkassa_test\TreeSorter\TreeSorter;
use Interkassa_test\WeightNode\WeightNode;
use PHPUnit\Framework\TestCase;
use Tree\Builder\NodeBuilder;

class TreeSorterTest extends TestCase
{

    public function treesProvider()
    {
        return [
            [
                (new NodeBuilder())->value(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, []), new WeightNode(1, [])]))
                    ->tree(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, []), new WeightNode(1, [])]))
                    ->end()->getNode(),
                5,
                (new NodeBuilder())->value(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, []), new WeightNode(1, [])]))
                    ->tree(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, []), new WeightNode(1, [])]))
                    ->end()->getNode(),
            ],
            [
                (new NodeBuilder())->value(LinkList::fromArray([new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(4, []), new WeightNode(3, [])]))
                    ->tree(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, [])]))
                    ->end()->getNode(),
                5,
                (new NodeBuilder())->value(LinkList::fromArray([new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(3, [])]))
                    ->tree(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, [])]))
                    ->end()->getNode(),
            ],
            [
                (new NodeBuilder())->value(LinkList::fromArray([new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(4, []), new WeightNode(3, [])]))
                    ->tree(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, [])]))
                    ->end()->getNode(),
                5,
                (new NodeBuilder())->value(LinkList::fromArray([new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(3, [])]))
                    ->tree(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(4, [])]))
                    ->leaf(LinkList::fromArray([new WeightNode(1, []), new WeightNode(1, [])]))
                    ->end()->getNode(),
            ],
        ];
    }

    /**
     * @param $testTree
     * @param $sumConst
     * @param $expect
     * @dataProvider treesProvider
     */
    public function testProcessNodes($testTree, $sumConst, $expect)
    {
        $sorter = new NodeWeightSumSorter($sumConst);
        $processor = new TreeSorter($sorter);
        $processor->processNodes($testTree);
        $this->assertEquals($expect, $testTree);

    }
}