<?php


namespace Interkassa_test\TreeSorter;


use Interkassa_test\LinkList\LinkList;
use Interkassa_test\WeightNode\WeightNode;
use Prophecy\Exception\InvalidArgumentException;
use Tree\Node\NodeInterface;

class NodeWeightSumSorter
{
    private $maxSumConst;

    /**
     * TreeNodeSumSorter constructor.
     * @param $maxSumConst
     */
    public function __construct($maxSumConst)
    {
        $this->maxSumConst = $maxSumConst;
    }


    public function processNode(NodeInterface $currentNode, NodeInterface $nextNode = null)
    {
        /** @var LinkList $nodesList */
        $nodesList = $currentNode->getValue();
        $nodesListAsArray = $nodesList->toArray();
        self::weightModesInsertSort($nodesListAsArray);
        list($toCurrentNode, $toNextNode) = self::chunkBySum($nodesListAsArray, $this->maxSumConst);
        $currentNode->setValue(LinkList::fromArray($toCurrentNode));
        // if has data to pass to next node - do it
        if ($toNextNode && $nextNode) {
            /** @var LinkList $nextNodesList */
            $nextNodesList = $nextNode->getValue();
            $nextNodesList->insertLastArray($toNextNode);
            $nextNode->setValue($nextNodesList);
        }
    }

    /**
     * @param WeightNode[] $array
     * @return array
     * Sort array of weight nodes using insert sort algorithm
     */
    public static function weightModesInsertSort(array &$array)
    {
        $length = count($array);
        for ($i = 1; $i < $length; $i++) {
            $element = $array[$i];
            if (!($element instanceof WeightNode)) {
                throw new InvalidArgumentException('Array element must be value of WeightNode type');
            }
            $j = $i;
            while ($j > 0 && $array[$j - 1]->getWeight() > $element->getWeight()) {
                $array[$j] = $array[$j - 1];
                $j = $j - 1;
            }
            $array[$j] = $element;
        }

        return $array;
    }

    /**
     * @param WeightNode[] $array
     * @param $sumConst
     * @return array
     * @throws \Exception
     * Explode array on two parts using sumConst.
     */
    public static function chunkBySum(array $array, $sumConst)
    {
        if (!$sumConst || !is_int($sumConst)) {
            throw new \Exception('sumConst must be integer');
        }
        $sum = 0;
        $sliceIndex = null;
        foreach ($array as $index => $weightNode) {
            if (!($weightNode instanceof WeightNode)) {
                throw new InvalidArgumentException('Array element must be value of WeightNode type');
            }
            $nodeWeight = $weightNode->getWeight();
            $sum += $nodeWeight;
            if ($sum > $sumConst) {
                $sliceIndex = $index;
                break;
            }
        }

        $sliceIndex = $sliceIndex ?? count($array);

        $toCurrentNode = array_slice($array, 0, $sliceIndex);
        $toNextNode = array_slice($array, $sliceIndex);

        return [$toCurrentNode, $toNextNode];
    }

}