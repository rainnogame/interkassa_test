<?php

namespace Interkassa_test\TreeSorter;

use Tree\Node\NodeInterface;
use Tree\Visitor\PreOrderVisitor;


/**
 * Class TreeSorter
 * @package Interkassa_test\Sorter
 * @property NodeWeightSumSorter $nodeSorter sort strategy for node
 */
class TreeSorter
{
    private $nodeSorter;

    function __construct(NodeWeightSumSorter $nodeSorter)
    {
        $this->nodeSorter = $nodeSorter;
    }


    /**
     * @param NodeInterface $root
     * Process all nodes by nodeSorter.
     */
    public function processNodes(NodeInterface &$root)
    {
        $visitor = new PreOrderVisitor();
        $nodesLinks = $visitor->visit($root);
        foreach ($nodesLinks as $index => $node) {
            $nextNode = $nodesLinks[$index + 1] ?? null;
            $this->nodeSorter->processNode($node, $nextNode);
        }
    }

}