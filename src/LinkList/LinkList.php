<?php

namespace Interkassa_test\LinkList;


/**
 * Class LinkList standard link list implementation
 * @package Interkassa_test\LinkList
 * @property ListNode $lastNode
 * @property ListNode $firstNode
 */
class LinkList
{
    private $firstNode;

    private $lastNode;

    private $count;


    /* List constructor */
    function __construct()
    {
        $this->firstNode = NULL;
        $this->lastNode = NULL;
        $this->count = 0;
    }

    public static function fromArray($data)
    {
        $list = new self();
        foreach ($data as $dataItem) {
            $list->insertLast($dataItem);
        }

        return $list;
    }

    public function insertLast($data)
    {
        if ($this->firstNode != NULL) {
            $link = new ListNode($data);
            $this->lastNode->setNext($link);
            $link->setNext(null);
            $this->lastNode = &$link;
            $this->count++;
        } else {
            $this->insertFirst($data);
        }
    }

    public function insertFirst($data)
    {
        $link = new ListNode($data);
        $link->setNext($this->firstNode);
        $this->firstNode = &$link;

        /* If this is the first node inserted in the list
           then set the lastNode pointer to it.
        */
        if ($this->lastNode == NULL)
            $this->lastNode = &$link;

        $this->count++;
    }


    public function getCount()
    {
        return $this->count;
    }

    public function toArray()
    {
        $listData = [];
        $current = $this->firstNode;

        while ($current != NULL) {
            array_push($listData, $current->readNode());
            $current = $current->getNext();
        }

        return $listData;
    }

    public function insertLastArray(array $toNextNode)
    {
        foreach ($toNextNode as $elem) {
            $this->insertLast($elem);
        }
    }

}