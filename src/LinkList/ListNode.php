<?php

namespace Interkassa_test\LinkList;
/**
 * Class ListNode linked list node implementation
 * @package Interkassa_test\LinkList\
 * @property ListNode $next
 */
class ListNode
{
    private $data;
    private $next;

    function __construct($data)
    {
        $this->data = $data;
        $this->next = NULL;
    }

    /**
     * @return ListNode
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param ListNode $next
     */
    public function setNext(ListNode $next = null)
    {
        $this->next = $next;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    function readNode()
    {
        return $this->data;
    }
}