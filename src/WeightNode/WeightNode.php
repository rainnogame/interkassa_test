<?php


namespace Interkassa_test\WeightNode;


use InvalidArgumentException;

/**
 * Class WeightNode
 * @package Interkassa_test\WeightNode
 * Implement linked list node with some data and weight for sorter @see TreeSorter. Weight has only integer value
 * @property integer weight
 */
class WeightNode
{
    private $weight;
    private $data;

    /**
     * WeightNode constructor.
     * @param $weight
     * @param $data
     */
    public function __construct($weight, $data)
    {
        $this->setWeight($weight);
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param integer $weight
     */
    public function setWeight($weight)
    {
        if (!is_int($weight)) {
            throw  new InvalidArgumentException('Node weight must be integer');
        }
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }


}